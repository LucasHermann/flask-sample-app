{
  description = "";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};

      PythonVer = pkgs.python310;
      PythonDeps = PythonPkgs:
        builtins.attrValues {
          inherit
            (PythonPkgs)
            black
            flask
            poetry
            setuptools
            ;
        };
      PythonEnv = PythonVer.withPackages PythonDeps;

      projName = "flask-sample-app";
    in rec {
      devShells = {
        default = pkgs.mkShell {
          packages = [
            PythonEnv
            pkgs.kind
            pkgs.kubectl
            pkgs.kubernetes-helm
          ];
        };
      };
    });
}
