# My Answers

## Build

### Pipeline Improvements

- Have automated tests that can be run as part of the pipeline before attempting to build
- Share Python dependencies between the steps of a build via a cache
- Instead of using Gitlab's `CI_REGISTRY_USER` and `CI_JOB_TOKEN`
- Share the application's version between the `pyprject.toml` and the container
- Skip the image build step when modifying files not impacting the image
  - e.g: Helm charts, README

### Dockerfile Improvements

- Add a `.dockerignore` to make sure that only the relevant elements are included
  - For example ingore `README.md`, `.gitlab-ci` and other
- Add an `EXPOSE` line to indicate which port (in the current case `8080`) the application listens.
- Move configurable elements to `ENV` blocks, such as:
  - port
  - poetry version

## Deployment

To deploy the Helm chart locally under the `app` name run:
```
helm package sample-app-workflow
helm install app sample-app-workflow
```

### Helm Chart Improvements

- Avoid duplicating the postgres varaibles in both the sample app and bd charts
- Investigate the need for delays to the probes (`initialDelaySeconds` and/or `periodSeconds`)
- Add TLS to the ingress config

